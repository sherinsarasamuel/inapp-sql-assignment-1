# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 17:33:17 2020

@author: user
"""
import sqlite3
conn = sqlite3.connect('students.db') # connect to the database
cursor = conn.cursor() # instantiate a cursor obj
hospital="""
CREATE TABLE HOSPITAL (
    Hospital_Id INTEGER NOT NULL PRIMARY KEY, 
    Hospital_Name TEXT NOT NULL, 
    Bed_Count INTEGER NOT NULL)"""

cursor.execute(hospital)
conn.commit()

hosp="INSERT INTO HOSPITAL (Hospital_Id, Hospital_Name,Bed_Count) VALUES(?,?,?)"
cursor.execute(hosp, ('1', 'Mayo Clinic', 200)) 
cursor.execute(hosp, ('2', 'Cleveland Clinic', 400))
cursor.execute(hosp, ('3', 'Johns Hopkins', 1000))
cursor.execute(hosp, ('4', 'UCLA Medical Center', 1500))
conn.commit()

cursor = conn.execute("SELECT * from HOSPITAL")
print(cursor.fetchall())
conn.close()