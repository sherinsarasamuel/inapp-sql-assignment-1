# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 18:55:56 2020

@author: user
"""


import sqlite3
conn = sqlite3.connect('students.db') # connect to the database
cursor = conn.cursor() # instantiate a cursor obj
doctor="""CREATE TABLE Doctor ( 
     Doctor_Id INTEGER NOT NULL PRIMARY KEY, 
     Doctor_Name TEXT NOT NULL, 
     Hospital_Id INTEGER NOT NULL, 
     Joining_Date TEXT NOT NULL, 
     Speciality TEXT NOT NULL, 
     Salary INTEGER NOT NULL,
     Experience INTEGER)"""
cursor.execute(doctor)
conn.commit()

doct="INSERT INTO Doctor (Doctor_Id, Doctor_Name, Hospital_Id, Joining_Date, Speciality, Salary, Experience) VALUES(?,?,?,?,?,?,?)" 
 
cursor.execute(doct, ('101', 'David', '1', '2005-2-10', 'Pediatric', '40000', 'NULL')) 
cursor.execute(doct, ('102', 'Michael', '1', '2018-07-23', 'Oncologist', '20000', 'NULL'))
cursor.execute(doct,('103', 'Susan', '2', '2016-05-19', 'Garnacologist', '25000', 'NULL'))
cursor.execute(doct,('104', 'Robert', '2', '2017-12-28', 'Pediatric ', '28000', 'NULL'))
cursor.execute(doct,('105', 'Linda', '3', '2004-06-04', 'Garnacologist', '42000', 'NULL')) 
cursor.execute(doct,('106', 'William', '3', '2012-09-11', 'Dermatologist', '30000', 'NULL')) 
cursor.execute(doct,('107', 'Richard', '4', '2014-08-21', 'Garnacologist', '32000', 'NULL')) 
cursor.execute(doct,('108', 'Karen', '4', '2011-10-17', 'Radiologist', '30000', 'NULL'))
conn.commit()

cursor = conn.execute("SELECT * from Doctor")
print(cursor.fetchall())
conn.close()