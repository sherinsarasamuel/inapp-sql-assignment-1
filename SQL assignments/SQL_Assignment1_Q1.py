# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 16:51:47 2020

@author: user
"""


import sqlite3
# Connect to sqlite database
conn = sqlite3.connect('students.db')
# cursor object
cursor = conn.cursor()
# drop query
cursor.execute("DROP TABLE IF EXISTS STUDENT")
# create query
query = """CREATE TABLE CARS1(
        NAME CHAR(20) NOT NULL, 
        OWNERNAME CHAR(20) NOT NULL )"""
cursor.execute(query)
# commit and close
conn.commit()
conn.execute("INSERT INTO CARS1 (NAME,OWNERNAME)" "VALUES('HONDA','SAM')")
conn.execute("INSERT INTO CARS1 (NAME,OWNERNAME)" "VALUES('MARUTHI','SANDRA')")
conn.execute("INSERT INTO CARS1 (NAME,OWNERNAME)" "VALUES('BMW','JOSE')")
conn.execute("INSERT INTO CARS1 (NAME,OWNERNAME)" "VALUES('XUV','WHITNEY')")
conn.execute("INSERT INTO CARS1 (NAME,OWNERNAME)" "VALUES('ALTO','PETER')")
conn.execute("INSERT INTO CARS1 (NAME,OWNERNAME)" "VALUES('ABC','NEAL')")
conn.execute("INSERT INTO CARS1 (NAME,OWNERNAME)" "VALUES('XYZ','ELLIE')")
conn.execute("INSERT INTO CARS1 (NAME,OWNERNAME)" "VALUES('JKL','JAMES')")
conn.execute("INSERT INTO CARS1 (NAME,OWNERNAME)" "VALUES('QWE','KATE')")
conn.execute("INSERT INTO CARS1 (NAME,OWNERNAME)" "VALUES('OIU','SARAH')")

conn.commit()
cursor = conn.execute("SELECT * from CARS1")
print(cursor.fetchall())
conn.close()
