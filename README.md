# Inapp SQL Assignment-1

Question 1

Write Python code to perform the following:

- Create a Table called Cars with two columns - Name of a car, Name of its owner.
- Add 10 records into the Cars table. Avoid SQL Injection.
- Retrieve all the records and print them.



Question 2

Write Python code to perform the following:

a. Create the two tables; Hospital, Doctor
b. Get the list of doctors as per the user inputted specialty and salary. Example : Pediatric doctors with salary above 30000.
c. Implement the functionality to fetch all the doctors as per the given Hospital Id. You must display the hospital name of a doctor.